<?php
namespace BlahArgh;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

/**
 * Wrapper object for the Slim framework to reduce and simplify their use.
 * 
 * Notes regarding route comparison:
 *      Slim takes the REQUEST_URI and removes the leading $request->getUri()->getBasePath()
 *      from it. The resulting URI is the one compared to the defined routes (it seems?).
 * 
 *      The base path is determined using the following (this is taken from
 *      https://dip.torproject.org/torproject/web/donate/blob/c260f4a7aa0c7fa58a051a9bad7b08e00433f100/vendor/slim/slim/Slim/Http/Uri.php
 *      starting at line 202, as of 2019-11-03):
 * 
 *          // Path
 *          $requestScriptName = parse_url($env->get('SCRIPT_NAME'), PHP_URL_PATH);
 *          $requestScriptDir = dirname($requestScriptName);
 * 
 *          // parse_url() requires a full URL. As we don't extract the domain name or scheme,
 *          // we use a stand-in.
 *          $requestUri = parse_url('http://example.com' . $env->get('REQUEST_URI'), PHP_URL_PATH);
 *
 *          $basePath = '';
 *          if (stripos($requestUri, $requestScriptName) === 0) {
 *              $basePath = $requestScriptName;
 *          } elseif ($requestScriptDir !== '/' && stripos($requestUri, $requestScriptDir) === 0) {
 *              $basePath = $requestScriptDir;
 *          }
 */
class SlimWrapper
{
    private $app;
    private $noMoreRoutes = false;       // Flag to determine if subsequent routes should even be loaded.
    private $groupMiddlewares = array(); // Storage for defined group middlewares.
    private $routes = array();           // Store the routes so they can be manipulated prior to
                                         // calling \Slim\App::run().
    private $lastDefinedRoute = null;    // Store the last defined route, so subsequent calls to
                                         // addRouteMiddleware know to which route to attach the middleware.
    private $subrootBase = null;         // Store the subroot base, if specified in the constructor.
    private $noMoreRenders = false;      // Flag to disable rendering more templates. This is mainly set after
                                         // calling redirectTo().
    private $attributes = array();       // Safe storage for variables that will need to be passed around
                                         // between different callbacks; for example, middleware to route.
    private $routingLog = array();       // Log of what files were loaded for the current route. Mainly for debugging.
    private $jsToAttach = array();       // JS file paths
    private $cssToAttach = array();      // CSS file paths
    private $areRoutesLoaded = false;    // Flag to determine if loadRoutes() was already called.

    public $request;       // Changes per route/middleware.
    public $response;      // Changes per route/middleware.
    public $basePath;      // Changes per route.
    public $next;          // Changes per middleware.
    public $wasNextCalled; // Changes per middleware.

    public $server;
    public $realURIDirectory;


    /**
     * Object instatiation.
     *
     * @param array $options Settings for this wrapper, details: [
     *    'documentRootAppend' => 'mysite'       - This will allow having multiple independent sites that
     *                                             sit under one domain server, by specifying a directory
     *                                             in the DOCUMENT_ROOT that will be appended as the
     *                                             new DOCUMENT_ROOT.
     *                                             If not passed in, STW will try to determine what it is
     *                                             by parsing $_SERVER['REQUEST_URI'].
     *    'slimObject'         => $slimObject    - If the Slim object (\Slim\App) was already created,
     *                                             it can be used by this wrapper.
     *    'subrootBase'        => 'mysubroots'   - This is used to specify a directory, relative to the
     *                                             root directory, as the location of all subroots.
     *                                             Defaults to "components".
     * ]
     */
    public function __construct(array $options = [])
    {
        $this->server = $this->encode($_SERVER);
        $this->server['ROOT_APPEND'] = '';
        // Set default options.
        if (!isset($options['documentRootAppend'])) {
            // Try to "intelligently" determine the document root.
            $parts = explode('/', dirname($this->server['SCRIPT_NAME']));
            if (isset($parts[1]) && $parts[1] != '') {
                $options['documentRootAppend'] = '/' . $parts[1];
            }
        }
        if (!isset($options['subrootBase'])) {
            $options['subrootBase'] = 'components';
        }
        // Process options.
        if (!empty($options['documentRootAppend'])) {
            // Allow specifying a directory in the DOCUMENT_ROOT to make into the new DOCUMENT_ROOT. This will allow
            // having multiple independent sites that sit under one domain server.
            // For example:
            //   $_SERVER['DOCUMENT_ROOT'] = '/var/www/html'
            //   $documentRoot = 'mysite'
            //   In this scenario, "mysite" will be the subdirectory to attach, so $this->server['DOCUMENT_ROOT'] will
            //   be set to "/var/www/html/mysite", and other server values will be adjusted accordingly.
            $documentRootAppend = str_replace('\\', '/', trim($options['documentRootAppend']));
            if (substr($documentRootAppend, 0, 1) !== '/') {
                $documentRootAppend = '/' . $documentRootAppend;
            }
            $this->server['ROOT_APPEND'] = $documentRootAppend;
            $this->server['DOCUMENT_ROOT'] = realpath($this->server['DOCUMENT_ROOT'] . $documentRootAppend);
            $length = strlen($documentRootAppend);
            if (substr($this->server['REQUEST_URI'], 0, $length) === $documentRootAppend) {
                $this->server['REQUEST_URI'] = substr($this->server['REQUEST_URI'], $length);
            }
            if (substr($this->server['SCRIPT_NAME'], 0, $length) === $documentRootAppend) {
                $this->server['SCRIPT_NAME'] = substr($this->server['SCRIPT_NAME'], $length);
            }
        }
        if (!empty($options['subrootBase'])) {
            $this->subrootBase = $options['subrootBase'];
            $this->subrootBase = str_replace('\\', '/', trim($this->subrootBase));
            if (substr($this->subrootBase, 0, 1) !== '/') {
                $this->subrootBase = '/' . $this->subrootBase;
            }
        }

        $this->server['DOMAIN_URI'] = 'http'
            . (!empty($this->server['HTTPS']) && $this->server['HTTPS'] === 'on' ? 's' : '')
            . '://' . $this->server['HTTP_HOST'];
        $this->server['BASE_PATH'] = $this->server['ROOT_APPEND'];
        $this->server['REQUEST_METHOD'] = strtolower($this->server['REQUEST_METHOD']);
        // Remove the query string from the request URI.
        $parts = explode('?', $this->server['REQUEST_URI']);
        if ($parts[0]) {
            // If there is any remaining path passed the document root to append, then clean it up.
            // Otherwise, leave it empty.
            // Request URI should be relative to the domain. Remove trailing "*" so user can't access a wildcard
            // route directly.
            $this->server['REQUEST_URI'] = '/' . trim($parts[0], '/*');
        } else {
            $this->server['REQUEST_URI'] = '';
        }
        if (!isset($this->server['QUERY_STRING'])) {
            $this->server['QUERY_STRING'] = '';
        }

        if (isset($this->server['HTTP_REFERER'])) {
            $length = strlen($this->server['DOMAIN_URI'] . $this->server['ROOT_APPEND']);
            if (substr($this->server['HTTP_REFERER'], 0, $length)
                === $this->server['DOMAIN_URI'] . $this->server['ROOT_APPEND']
            ) {
                // Referer was from the same domain.
                $this->server['REFERRER'] = substr($this->server['HTTP_REFERER'], $length);
            } else {
                // Referer was from an outside domain.
                // Leave it as is.
                $this->server['REFERRER'] = $this->server['HTTP_REFERER'];
            }
        } else {
            $this->server['REFERRER'] = '';
        }

        $this->routingLog[] = "ORIG REQUESTED URI: {$_SERVER['REQUEST_URI']}";
        $this->routingLog[] = "REQUESTED URI: {$this->server['REQUEST_URI']}";
        $this->routingLog['_SERVER'] = $_SERVER;
        $this->realURIDirectory = $this->getRealDirectory(); //<-- "/" or "/some/path"

        if (!empty($options['slimObject']) && is_a($options['slimObject'], '\Slim\App')) {
            $this->slim = $options['slimObject'];
            $this->container = $this->slim->getContainer();
        } else {
            $this->container = new \Slim\Container();
            $this->slim = new \Slim\App($this->container);
        }
    }

    /**
     * Get the existing directory out of a path. This is useful if you want to have index.php include files that are in
     * a matching base directory.
     *
     * @param string $string The directory path to test.
     * @return string The real directory found.
     */
    private function getRealDirectory(string $string = null)
    {
        if ($string === null) {
            $string = $this->server['REQUEST_URI'];
        }

        $string = str_replace('\\', '/', trim($string));
        $tokens = explode('/', trim($string, '/'));
        $dir = null;
        foreach ($tokens as $part) {
            if ($part == '') {
                continue;
            }
            $check = "$dir/$part";
            $this->routingLog[] = "CHECKING FOR REAL DIRECTORY: " . $this->server['DOCUMENT_ROOT'] . $this->subrootBase . $check;
            if (is_dir($this->server['DOCUMENT_ROOT'] . $this->subrootBase . $check)) {
                $dir = $check;
            } else {
                break;
            }
        }
        return $dir;
    }

    /**
     * Convert the passed in callback into a proper middleware callback.
     *
     * @param callable $callback The function to run.
     * @param string $path The path for this callback if converting a group middleware.
     * @return callable Properly formatted callback.
     */
    private function makeMiddlewareCallback(callable $callback, string $path = null)
    {
        if ($callback instanceof \Closure) {
            $wrapper = $this;
            $middlewareCallback = $callback->bindTo($wrapper);
            $middlewareCall = function ($request, $response, $next) use ($wrapper, $middlewareCallback, $path) {
                switch ($path) {
                    case '--ROOT--':
                        $wrapper->routingLog[] = "------------------ RUNNING ROOT MIDDLEWARE";
                        break;
                    case '--ROUTE--':
                        $wrapper->routingLog[] = "------------------ RUNNING ROUTE MIDDLEWARE";
                        break;
                    default:
                        $wrapper->routingLog[] = "------------------ RUNNING MIDDLEWARE \"$path\"";
                }
                $wrapper->request = $request;
                $wrapper->response = $response;
                $wrapper->next = $next;
                $wrapper->wasNextCalled = false;
                $callNext = function () use ($wrapper) {
                    $wrapper->response = call_user_func($wrapper->next, $wrapper->request, $wrapper->response);
                    $wrapper->wasNextCalled = true;
                };
                if (!empty($wrapper->request->getAttribute('routeInfo'))
                    && !empty($wrapper->request->getAttribute('routeInfo')[2])
                ) {
                    $args = $wrapper->request->getAttribute('routeInfo')[2];
                } else {
                    $args = array();
                }
                $return = $middlewareCallback($callNext, $args);
                // If them middleware returns a FALSE or a Response object, then don't call Next and just end the
                // process. This allows properly ending the process in the middleware, like when page permission
                // is not met so the user is redirected from the middleware.
                if (!$wrapper->wasNextCalled && $return !== false && !is_a($return, get_class($wrapper->response))) {
                    $callNext();
                }
                return $wrapper->response;
            };
            return $middlewareCall;
        } else {
            return $callback;
        }
    }


    /**
     * Add an attribute that can then be retrieved in another callback.
     * For example, passing a paramenter from a middleware to a route.
     *
     * @param string $name The name of the attribute to set.
     * @param mixed $value The value for the attribute.
     */
    public function addAttribute(string $name, $value)
    {
        $this->attributes[$name] = $value;
    }

    /**
     * Add a dependency injection into the container.
     *
     * @param string $name The name for the dependency.
     * @param callable $callback The callback function to call for the dependency.
     * @return self Return this object for chaining.
     */
    public function addDependency(string $name, callable $callback)
    {
        $c = $this->slim->getContainer();
        $c[$name] = $callback->bindTo($this->slim, $this->slim);

        return $this;
    }

    /**
     * Define a middleware to use for a group.
     *
     * @param string $path The path for the middleware.
     * @param callable $callback The middleware function to run.
     * @return self Return this object for chaining.
     */
    public function addGroupMiddleware(string $path, callable $callback)
    {
        if (!isset($this->groupMiddlewares[$path])) {
            $this->groupMiddlewares[$path] = array();
        }
        $this->groupMiddlewares[$path][] = $this->makeMiddlewareCallback($callback, $path);
        $this->routingLog[] = "ADD GROUP MIDDLEWARE: $path";
        return $this;
    }

    /**
     * Add a middleware.
     *
     * @param callable $callback The middleware function to run.
     * @param string $logLabel (optional) The label to display in the logs when this middleware is added.
     * @return self Return this object for chaining.
     */
    public function addMiddleware(callable $callback, string $logLabel = null)
    {
        if (empty($logLabel)) {
            $logLabel = '--ROOT--';
        }
        $this->slim->add($this->makeMiddlewareCallback($callback, $logLabel));
        $this->routingLog[] = "ADD MIDDLEWARE: $logLabel";
        return $this;
    }

    /**
     * Define a middleware to use for the last defined route.
     *
     * @param callable $callback The middleware function to run.
     * @return self Return this object for chaining.
     */
    public function addRouteMiddleware(callable $callback)
    {
        if (!$this->lastDefinedRoute) {
            return this;
        }
        $logLabel = "ROUTE: {$this->lastDefinedRoute->pattern}";
        $this->lastDefinedRoute->add($this->makeMiddlewareCallback($callback, $logLabel));
        $this->routingLog[] = "ADD ROUTE MIDDLEWARE: $logLabel";
        return $this;
    }

    /**
     * This is used to encode a string so that it is safe for print out.
     * This can be overwritten if a different encoding process is desired.
     *
     * @param string|array $strOrArray The string or array to encode.
     * @return string|array The encoded string or array.
     */
    public function encode($strOrArray)
    {
        if (is_array($strOrArray)) {
            $output = array();
            foreach ($strOrArray as $name => $value) {
                $output[htmlentities($name, ENT_QUOTES | ENT_SUBSTITUTE)]
                    = htmlentities($value, ENT_QUOTES | ENT_SUBSTITUTE);
            }
            return $output;
        } else {
            return htmlentities($strOrArray, ENT_QUOTES | ENT_SUBSTITUTE);
        }
    }

    /**
     * Get an attribute that may have been set from another callback.
     * For example, passing a paramenter from a middleware to a route.
     *
     * @param string $name The name of the attribute to get.
     * @return mixed The value of the attribute.
     */
    public function getAttribute(string $name)
    {
        return isset($this->attributes[$name]) ? $this->attributes[$name] : null;
    }

    /**
     * Get the CSS files to attach.
     *
     * @return array The determined CSS files to attach.
     */
    public function getCssToAttach()
    {
        return $this->cssToAttach;
    }

    /**
     * Get the JS files to attach.
     *
     * @return array The determined JS files to attach.
     */
    public function getJsToAttach()
    {
        return $this->jsToAttach;
    }

    /**
     * Get an input parameter first from PUT, then POST, then GET, and if not found, NULL is returned.
     *
     * @param string $name The name of the param to get.
     * @return mixed The value of the parameter if found, NULL otherwise.
     */
    public function getParam(string $name)
    {
        if (empty($this->request)) {
            return null;
        }
        return $this->request->getParam($name);
    }

    /**
     * Get all input parameters.
     *
     * @return array Array of all the params.
     */
    public function getParams()
    {
        if (empty($this->request)) {
            return array();
        }
        return $this->request->getParams();
    }

    /**
     * Get the routing log.
     *
     * @return array The array of routing logs for debugging purposes.
     */
    public function getRoutingLog()
    {
        return $this->routingLog;
    }

    /**
     * Get the uploaded files. This is a shortcut for $this->request->getUploadedFiles(), which returns and array with
     * the key being the field name and the value being a Slim\Http\UploadedFile object.
     *
     * Example:
     *    Array
     *    (
     *        [Filedata] => Slim\Http\UploadedFile Object
     *            (
     *                [file] => C:\Users\santos.134\AppData\Local\Temp\1\phpF836.tmp
     *                [name:protected] => test_upload_g.txt
     *                [type:protected] => text/plain
     *                [size:protected] => 1540
     *                [error:protected] => 0
     *                [sapi:protected] => 1
     *                [stream:protected] =>
     *                [moved:protected] =>
     *            )
     *    )
     *
     * @return array An array of data regarding the uploaded file.
     */
    public function getUploadedFiles()
    {
        if (empty($this->request)) {
            return array();
        }
        return $this->request->getUploadedFiles();
    }

    /**
     * Get environment variables.
     *
     * @return array An array of variables currently set in this object. This is mainly for debugging purposes.
     */
    public function getVars()
    {
        return array(
            'realURIDirectory' => $this->realURIDirectory,
        ) + $this->server;
    }

    /**
     * Load the routes.
     */
    public function loadRoutes()
    {
        $app = $this;
        // Add routes if defined in a "routes.php" file in a real directory that is a part of the URL.
        $routesFile = 'routes.php';
        // Always load the routes file in the subrootBase, if it exists.
        // Then check other routes file based off of the real directories within the URI.
        $this->routingLog[] = "Real URI Directory: $this->realURIDirectory";
        // Load routes in all ancestors under the current subroot, but not the root.
        $parts = explode('/', $this->realURIDirectory);
        // Add the leading '/' back so it can be appended to the path nicely, where if the part is blank,
        // there won't be a double slash in the path.
        foreach ($parts as $i => $v) {
            if ($v == '') {
                continue;
            }
            $parts[$i] = "/$v";
        }
        // Include an empty part so the subrootBase also gets processed.
        $this->routingLog[] = "Path parts: <pre style=\"padding-left:60px;\">" . print_r($parts, true) . "</pre>";
        $dir = "{$this->server['DOCUMENT_ROOT']}";
        $rel = "{$this->subrootBase}";
        $foundSubrootRoute = false;
        foreach ($parts as $part) {
            $this->routingLog[] = "CHECKING FOR ROUTE FILE IN \"$dir$rel$part\"";
            if (file_exists("$dir$rel$part/$routesFile")) {
                $this->routingLog[] = "ROUTES FILE LOADED: $dir$rel$part/$routesFile";
                include "$dir$rel$part/$routesFile";
                if ($part) {
                    $foundSubrootRoute = true;
                }
            }
            if (file_exists("$dir$rel$part/path.js")) {
                $this->jsToAttach[] = "$rel$part/path.js";
            }
            if (file_exists("$dir$rel$part/path.css")) {
                $this->cssToAttach[] = "$rel$part/path.css";
            }
            // Append the next part to traverse the path, but only if $part is not empty (subroot base part).
            if ($part) {
                $rel .= "$part";
            }
        }
        if (file_exists("$dir$rel/here.js")) {
            $this->jsToAttach[] = "$rel/here.js";
        }
        if (file_exists("$dir$rel/here.css")) {
            $this->cssToAttach[] = "$rel/here.css";
        }
        // Set flag to not load further routes so root routes does not conflict with subroot routes that
        // were just loaded.
        if ($foundSubrootRoute) {
            $this->noMoreRoutes = true;
        }

        // Add routes if defined in a "routes.php" file in the base directory, but only if there were no subroot
        // route files already loaded.
        if (!$this->noMoreRoutes && file_exists("{$this->server['DOCUMENT_ROOT']}/routes.php")) {
            $this->routingLog[] = "ROOT ROUTE LOADED: {$this->server['DOCUMENT_ROOT']}/routes.php";
            include "{$this->server['DOCUMENT_ROOT']}/routes.php";
        }

        // Attach group middlewares to the proper routes at this point.
        foreach ($this->routes as $rPath => $route) {
            if (!isset($route->middlewaresToAttach)) {
                continue;
            }
            foreach ($route->middlewaresToAttach as $mPath => $middlewareCallback) {
                $route->add($middlewareCallback);
                $this->routingLog[] = "GROUP MIDDLEWARE ATTACHED TO \"$rPath\": $mPath";
            }
        }
        // Set flag.
        $this->areRoutesLoaded = true;
    }

    /**
     * Specify a redirection.
     *
     * @param string $uri The URI to redirect to.
     * @return object The response object.
     */
    public function redirectTo(string $uri)
    {
        // If leading with "http://" or "https://", then redirect as is.
        if (substr($uri, 0, 7) === 'http://' || substr($uri, 0, 8) === 'https://') {
            $this->response = $this->response->withRedirect($uri);
        } else {
            // If not leading with a slash ('/'), redirect based off of the
            // realURIDirectory, so it behaves similar to routes.
            if (substr($uri, 0, 1) !== '/' && $this->realURIDirectory !== '/') {
                $uri = $this->realURIDirectory . '/' . $uri;
            }
            // Make sure the URI has a leading slash ('/') before it's appended
            // to the BASE_PATH (since the code above will have a leading slash.
            if (substr($uri, 0, 1) !== '/') {
                $uri = '/' . $uri;
            }
            $this->response = $this->response->withRedirect($this->server['BASE_PATH'] . $uri);
        }
        $this->noMoreRenders = true;
        return $this->response;
    }

    /**
     * Define a route.
     *
     * @param string $methods The method(s) to use for the route.
     * @param string $path The path for the route.
     * @param callable $callback The callback function to run for the route.
     * @return self Return this object for chaining.
     */
    public function route(string $methods, string $path, callable $callback)
    {
        if ($this->noMoreRoutes) {
            return $this;
        }
        if (!is_string($path)) {
            return $this;
        }

        // Declare the route as coming from the subroot if the path does not
        // have a leading slash ('/') and realURIDirectory is specified.
        if (substr($path, 0, 1) !== '/' && $this->realURIDirectory !== '/') {
            $path = $this->realURIDirectory . '/' . $path;
        }

        // Make sure the path to make into a route has a leading slash ('/')
        // before it is passed in to Slim.
        if (substr($path, 0, 1) !== '/') {
            $path = '/' . $path;
        }

        // Trim out the trailing slash '/', otherwise the URI needs to also
        // have the slash for the route to be found.
        // But routes that are just '/' itself, should not be trimmed.
        if ($path !== '/') {
            $path = rtrim($path, '/');
        }

        $methods = explode(',', $methods);
        foreach ($methods as $i => $m) {
            $m = trim($m);
            if ($m == '') {
                continue;
            }
            $methods[$i] = strtoupper($m);
        }
        $wrapper = $this;
        $routeCallback = $callback->bindTo($wrapper);
        $responseCall = function ($request, $response, $args) use ($wrapper, $routeCallback, $path) {
            $wrapper->routingLog[] = "------------------ RUNNING ROUTE \"$path\"";
            $wrapper->request = $request;
            $wrapper->response = $response;
            $routeCallback($args);
            return $wrapper->response;
        };

        $pathID = implode(',', $methods) . '--' . $path;
        $this->routingLog[] = "ADD ROUTE: \"$pathID\" using the path of \"$path\"";
        $route = $this->slim->map($methods, $path, $responseCall);
        // Check for any group middlewares and store them to this route.
        // They will be attached prior to running \Slim\App::run(), so their
        // order of execution is similar to how Slim does it.
        // Group middleware of an ancestor path will need to be loaded
        // first (which is reverse of just adding them as they are declared),
        // but those within the same path will be loaded similar to Slim.
        $groupMiddlewares = array_reverse($this->groupMiddlewares);
        $route->middlewaresToAttach = array();
        foreach ($groupMiddlewares as $gPath => $middlewareCallbacks) {
            if (substr($path, 0, strlen($gPath)) === $gPath) {
                $this->routingLog[] = "GROUP MIDDLEWARE TO ATTACH TO \"$pathID\": $gPath";
                foreach ($middlewareCallbacks as $i => $middlewareCallback) {
                    $route->middlewaresToAttach["$gPath-$i"] = $middlewareCallback;
                }
            }
        }
        // Store this route so it can be referenced later.
        $this->routes[$pathID] = $route;
        // Store this route so subsequent calls to addRouteMiddleware know to attach it to this route.
        $this->lastDefinedRoute = $route;

        return $this;
    }

    /**
     * Run the slim process.
     */
    public function run()
    {
        // If the routes have not yet been loaded, then load it now.
        if (!$this->areRoutesLoaded) {
            $this->loadRoutes();
        }
        // Run Slim.
        $this->slim->run();
    }

    /**
     * Set/Get settings.
     *
     * @param string $name The name of the settings to set or get.
     * @param mixed $value (optional) The value to set.
     * @return mixed The setting value if $value is NULL, otherwise returns this object for chaining.
     */
    public function settings(string $name, $value = null)
    {
        $settings = $this->container->get('settings');
        if ($value === null) {
            return isset($settings[$name]) ? $settings[$name] : null;
        } else {
            $settings[$name] = $value;
            return $this;
        }
    }

    /**
     * Modify the response object to return JSON.
     *
     * @param mixed $data The data to pass in as JSON.
     * @param integer $status The status to set for the response.
     * @param integer $encodingOptions The encoding options to pass along.
     */
    public function withJson($data, int $status = 200, int $encodingOptions = 0)
    {
        $this->response = $this->response->withJson($data, $status, $encodingOptions);
    }

    /**
     * Set the status code for the response object.
     *
     * @param integer $code The response code to return.
     * @param string $reasonPhrase (optional) The string to pass along with the response.
     */
    public function withStatus(int $code, string $reasonPhrase = '')
    {
        $this->response = $this->response->withStatus($code, $reasonPhrase);
    }

    /**
     * Shortcut to write out to the Response object if it exists, to the output buffer otherwise.
     *
     * @param string $str The string to write out.
     */
    public function write(string $str)
    {
        if (!empty($this->response) && method_exists($this->response, 'write')) {
            $this->response->write($str);
        } else {
            print $str;
        }
    }
}
